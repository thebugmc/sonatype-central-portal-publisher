plugins {
    id("com.gradle.plugin-publish") version "1.2.1"
}

group = "net.thebugmc.gradle"
version = "1.2.4"

java.toolchain.languageVersion.set(JavaLanguageVersion.of(17))

repositories {
    google()
    mavenCentral()
}

dependencies {
    compileOnly("com.android.library:com.android.library.gradle.plugin:8.2.2")
    compileOnly("org.jetbrains.dokka:dokka-gradle-plugin:1.9.10")
}

gradlePlugin {
    website = "https://gitlab.com/thebugmc/sonatype-central-portal-publisher"
    vcsUrl = "https://gitlab.com/thebugmc/sonatype-central-portal-publisher"

    plugins.create("sonatype-central-portal-publisher") {
        id = "net.thebugmc.gradle.sonatype-central-portal-publisher"
        implementationClass = "net.thebugmc.gradle.sonatypepublisher.SonatypeCentralPortalPublisherPlugin"
        displayName = "Sonatype Central Portal Publisher"
        description = "Gradle plugin for building and uploading bundles to the Sonatype Central Portal."
        tags = listOf("maven", "maven-central", "publish", "sonatype")
    }
}

tasks {
    create("generateJavaVersionBadge") {
        doLast {
            val version = java.toolchain.languageVersion.get().asInt()
            val output = File("java-version-badge.json")
            output.writeText(
                """{
                |    "schemaVersion": 1,
                |    "label": "Java",
                |    "message": "$version",
                |    "color": "#437291"
                |}""".trimMargin()
            )
        }
    }
    javadoc {
        options {
            this as StandardJavadocDocletOptions
            addBooleanOption("Xdoclint:all,-missing", true)
        }
    }
    compileJava {
        finalizedBy("generateJavaVersionBadge")
    }
}
