package net.thebugmc.gradle.sonatypepublisher.platform;

import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.UnknownDomainObjectException;
import org.gradle.api.component.SoftwareComponent;
import org.gradle.api.file.FileTree;
import org.gradle.api.logging.Logger;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.bundling.Zip;

/**
 * Platform helps the plugin figure out the differences between various kinds of projects (thanks
 * Android for making a very different platform).
 */
public interface Platform {
    /**
     * {@link FileTree} that represents all sources of a {@code main} task.
     */
    FileTree mainSourceSetSources(Logger log, Project target);

    /**
     * The default POM {@code packaging} value (e.g. {@code jar}, {@code aar}, etc).
     */
    String defaultPackaging();

    /**
     * Task that generates the main artifact.
     */
    Zip jarTask(Logger log, TaskContainer tasks);

    /**
     * Task that generates the {@code ...-javadoc.jar} artifact.
     *
     * @throws UnknownDomainObjectException When some platform is trying to find the task.
     */
    Task javadocTask(
        Logger log,
        Project target,
        TaskContainer tasks,
        FileTree mainSourceSetSources,
        Provider<String> nameProvider
    ) throws UnknownDomainObjectException;

    /**
     * Something like {@code components.java} or its alternatives.
     *
     * @throws UnknownDomainObjectException When the platform is supposed to search for a necessary
     *                                      component but is unable to find it.
     */
    SoftwareComponent component(Logger log, Project target) throws UnknownDomainObjectException;
}
