package net.thebugmc.gradle.sonatypepublisher;

/**
 * Determines whether to publish the artifact immediately, or to hold it back behind Publish button
 * in the Sonatype Central Portal panel.
 *
 * @see #AUTOMATIC
 * @see #USER_MANAGED
 */
public enum PublishingType {
    /**
     * The Publish button will be pressed for you immediately after a successful validation. This is
     * the default.
     */
    AUTOMATIC,

    /**
     * The Publish button will be there for you to press in the Sonatype Central Portal panel.
     */
    USER_MANAGED,
}
