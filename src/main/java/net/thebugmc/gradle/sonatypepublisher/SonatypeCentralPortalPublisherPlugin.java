package net.thebugmc.gradle.sonatypepublisher;

import net.thebugmc.gradle.sonatypepublisher.internal.DefaultCentralPortalExtension;
import net.thebugmc.gradle.sonatypepublisher.platform.AndroidLibraryPlatform;
import net.thebugmc.gradle.sonatypepublisher.platform.JavaPlatform;
import net.thebugmc.gradle.sonatypepublisher.platform.Platform;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.UnknownDomainObjectException;
import org.gradle.api.file.RelativePath;
import org.gradle.api.logging.Logger;
import org.gradle.api.plugins.BasePluginExtension;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.provider.Provider;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.MavenPublication;
import org.gradle.api.publish.maven.internal.publication.MavenPublicationInternal;
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.bundling.Zip;
import org.gradle.plugins.signing.Sign;
import org.gradle.plugins.signing.SigningExtension;
import org.gradle.plugins.signing.SigningPlugin;

import java.util.concurrent.Callable;

/**
 * Gradle plugin for building and uploading bundles to the Sonatype Central Portal.
 */
public class SonatypeCentralPortalPublisherPlugin implements Plugin<Project> {
    public static final String GROUP = "central portal publishing";

    /**
     * Determines platform based on the plugins already applied - for example,
     * {@code com.android.library}.
     */
    public static Platform detectPlatform(Logger log, Project target) {
        log.info("Detecting the Gradle project platform");
        if (target.getPlugins().hasPlugin("com.android.library"))
            return new AndroidLibraryPlatform(log, target);
        // TODO consider putting JavaPlatform under `java` plugin
        // TODO think about Kotlin support
        return new JavaPlatform();
    }

    /**
     * Applies necessary plugins for this plugin ({@code maven-publish}, {@code signing}, etc).
     */
    public static void applyNecessaryPlugins(Logger log, Project target) {
        log.info("Applying the `maven-publish` and `signing` plugins");
        var pluginManager = target.getPluginManager();
        pluginManager.apply(MavenPublishPlugin.class);
        pluginManager.apply(SigningPlugin.class);
    }

    /**
     * Sets up {@link CentralPortalExtension}.
     *
     * @throws IllegalArgumentException When the extension is already set up.
     */
    public static CentralPortalExtension setupExtension(
        Logger log,
        ExtensionContainer extensions
    ) throws IllegalArgumentException {
        log.info("Setting up the `centralPortal` extension");
        return extensions.create("centralPortal", DefaultCentralPortalExtension.class);
    }

    /**
     * Result of the {@link #setupJarTasks} method.
     *
     * @param sourcesJarTask {@code `:sourcesJar`} task.
     * @param javadocJarTask {@code `:javadocJar`} task.
     */
    public record JarTasks(
        Jar sourcesJarTask,
        Jar javadocJarTask
    ) {
    }

    /**
     * Sets up {@code `:sourcesJar`} and {@code `:javadocJar`} tasks.
     *
     * @throws InvalidUserDataException When any of those tasks already exist.
     */
    public static JarTasks setupJarTasks(
        Logger log,
        Platform platform,
        Project target,
        TaskContainer tasks,
        Provider<String> nameProvider
    ) throws InvalidUserDataException {
        var mainSources = platform.mainSourceSetSources(log, target);

        log.info("Setting up the `:sourcesJar` task");
        var sourcesJarTask = tasks.create("sourcesJar", Jar.class);
        sourcesJarTask.setGroup(GROUP);
        sourcesJarTask.setDescription("Creates a `-sources` jar.");
        sourcesJarTask.getArchiveClassifier().set("sources");
        sourcesJarTask.from(mainSources);

        var javadocTask = platform.javadocTask(log, target, tasks, mainSources, nameProvider);

        log.info("Setting up the `:javadocJar` task");
        var javadocJarTask = tasks.create("javadocJar", Jar.class);
        javadocJarTask.setGroup("central portal publishing");
        javadocJarTask.setDescription("Creates a `-javadoc` jar");
        javadocJarTask.getArchiveClassifier().set("javadoc");
        javadocJarTask.from(javadocTask);

        return new JarTasks(sourcesJarTask, javadocJarTask);
    }

    /**
     * Sets up a {@code centralPortal} publication.
     *
     * @throws UnknownDomainObjectException If {@link PublishingExtension} has not been set up yet.
     * @throws InvalidUserDataException     If a publication named {@code centralPortal} already
     *                                      exists.
     */
    public static MavenPublication setupPublication(
        Logger log,
        Project target,
        Platform platform,
        ExtensionContainer extensions,
        TaskContainer tasks,
        JarTasks jarTasks,
        DefaultCentralPortalExtension centralPortalExtension,
        Provider<String> nameProvider
    ) throws UnknownDomainObjectException, InvalidUserDataException {
        log.info("Setting up the `centralPortal` publication");
        var publishingExtension = extensions.getByType(PublishingExtension.class);
        var publications = publishingExtension.getPublications();

        var mavenCentralPortalPublication = publications
            .create("centralPortal", MavenPublication.class);

        target.afterEvaluate( // good enough
            project -> {
                // artifact id
                var name = nameProvider.get();
                mavenCentralPortalPublication.setArtifactId(name);

                // artifacts
                var jarTask = centralPortalExtension.getJarTask().getOrNull();
                if (jarTask == null) jarTask = platform.jarTask(log, tasks);
                mavenCentralPortalPublication
                    .artifact(jarTask);
                mavenCentralPortalPublication
                    .artifact(
                        centralPortalExtension
                            .getSourcesJarTask()
                            .getOrElse(jarTasks.sourcesJarTask)
                    );
                mavenCentralPortalPublication
                    .artifact(
                        centralPortalExtension
                            .getJavadocJarTask()
                            .getOrElse(jarTasks.javadocJarTask)
                    );

                // dependencies
                mavenCentralPortalPublication
                    .from(platform.component(log, target));
                mavenCentralPortalPublication
                    .versionMapping(centralPortalExtension::configureVersionMapping);

                // Fix for #3, Gradle generates `module.json` with `from(javaComponent)`, but the
                //  Central Portal API is not too keen on having custom files other than what is
                //  described in the manual publishing documentation for making bundles.
                //  Looking at the Gradle's source, this `null` seems to make it just remove the
                //  `module.json` file, while also not breaking horribly. This solution, however, is
                //  quite shaky, and may break in the future Gradle versions. Brace yourselves.
                //noinspection DataFlowIssue
                ((MavenPublicationInternal) mavenCentralPortalPublication)
                    .setModuleDescriptorGenerator(null);

                // pom
                mavenCentralPortalPublication.pom(pom -> {
                    pom.getName().set(name);
                    pom.getDescription().set(target.getDescription());
                    pom.setPackaging(platform.defaultPackaging());
                    centralPortalExtension.configurePom(pom);
                });
            }
        );

        return mavenCentralPortalPublication;
    }

    /**
     * Applies signing to the publication and grabs the resulting task.
     *
     * @throws UnknownDomainObjectException When {@link SigningExtension} has not been loaded yet.
     */
    public static Sign setupSigning(
        Logger log,
        Project target,
        MavenPublication mavenCentralPortalPublication
    ) throws UnknownDomainObjectException {
        log.info("Setting up the signing of the `centralPortal` publication");
        var signingExtension = target.getExtensions()
            .getByType(SigningExtension.class);
        return signingExtension.sign(mavenCentralPortalPublication).get(0);
    }

    /**
     * Sets up the {@code :generateBundle} and {@code :checksumBundle} tasks, returning the latter.
     *
     * @throws InvalidUserDataException When {@code :generateBundle} or {@code :checksumBundle}
     *                                  task names are already taken.
     */
    public static Checksum setupBundleTasks(
        Logger log,
        Project target,
        TaskContainer tasks,
        MavenPublicationInternal publication,
        Provider<String> nameProvider
    ) throws InvalidUserDataException {
        log.info("Setting up the `:generateBundle` task");
        var generateBundleTask = tasks.create("generateBundle", Zip.class);
        generateBundleTask.setGroup(GROUP);
        generateBundleTask.setDescription(
            "Creates a bundle for Sonatype Central Portal publishing."
        );
        generateBundleTask.getArchiveClassifier().set("bundle");
        generateBundleTask.into((Callable<RelativePath>) () -> {
            var groupParts = (target.getGroup() + "").split("\\.");
            var name = nameProvider.get();
            var version = target.getVersion() + "";
            return new RelativePath(false, groupParts)
                .append(false, name, version);
        });

        var publishableMavenArtifacts = publication.getPublishableArtifacts().getFiles();
        generateBundleTask.from(publishableMavenArtifacts, file -> file.rename(n -> n.replace(
            "pom-default.xml",
            nameProvider.get() + "-" + target.getVersion() + ".pom"
        )));

        var bundlesDir = target.getLayout().getBuildDirectory().dir("bundles");
        generateBundleTask.getDestinationDirectory().set(bundlesDir);

        var checksumTask = tasks.create("checksumBundle", Checksum.class);
        checksumTask.setGroup(GROUP);
        checksumTask.setDescription(
            "Generates `.md5` and `.sha1` checksums of the `:generateBundle` output."
        );
        checksumTask.calculateFor(generateBundleTask);
        return checksumTask;
    }

    /**
     * Sets up the {@code :publishToCentralPortal} task.
     */
    public static void setupPublishToCentralPortalTask(
        Logger log,
        TaskContainer tasks,
        Checksum checksumTask
    ) {
        log.info("Setting up the `:publishToCentralPortal` task");
        var publishToCentralPortalTask = tasks.create(
            "publishToCentralPortal",
            PublishToCentralPortal.class
        );
        publishToCentralPortalTask.setGroup(GROUP);
        publishToCentralPortalTask.setDescription(
            "Publishes the bundle to Sonatype Central Portal"
        );
        publishToCentralPortalTask.upload(checksumTask);
    }

    public void apply(Project target) {
        var log = target.getLogger();
        applyNecessaryPlugins(log, target);
        var platform = detectPlatform(log, target);
        var extensions = target.getExtensions();
        var centralPortalExtension = (DefaultCentralPortalExtension) setupExtension(
            log,
            extensions
        );

        var basePluginExtension = extensions.getByType(BasePluginExtension.class);
        var nameProvider = centralPortalExtension.getName()
            .orElse(target.getRootProject().getName());
        target.afterEvaluate(
            ignored -> basePluginExtension
                .getArchivesName()
                .set(nameProvider.get())
        );

        var tasks = target.getTasks();
        var jarTasks = setupJarTasks(log, platform, target, tasks, nameProvider);
        var mavenCentralPortalPublication = setupPublication(
            log, target, platform, extensions, tasks, jarTasks, centralPortalExtension, nameProvider
        );
        setupSigning(log, target, mavenCentralPortalPublication);

        var checksumTask = setupBundleTasks(
            log, target, tasks,
            (MavenPublicationInternal) mavenCentralPortalPublication,
            nameProvider
        );
        setupPublishToCentralPortalTask(log, tasks, checksumTask);
    }
}
