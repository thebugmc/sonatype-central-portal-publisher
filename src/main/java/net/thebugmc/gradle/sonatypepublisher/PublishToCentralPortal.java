package net.thebugmc.gradle.sonatypepublisher;

import org.gradle.api.DefaultTask;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.TaskAction;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Objects;

import static java.lang.Math.min;
import static java.lang.System.currentTimeMillis;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Task for publishing to Central Portal via Central API.
 */
public abstract class PublishToCentralPortal extends DefaultTask {
    /**
     * Central API URL for publishingType being {@code AUTOMATIC}.
     */
    public static final String UPLOAD_ENDPOINT
        = "https://central.sonatype.com/api/v1/publisher/upload?publishingType=";

    private Checksum checksumTask;

    /**
     * Mark what file should be uploaded.
     */
    public void upload(Checksum checksumTask) {
        if (this.checksumTask != null && checksumTask != this.checksumTask)
            throw new IllegalArgumentException("Checksum task already set to " + this.checksumTask);

        dependsOn(checksumTask);
        this.checksumTask = checksumTask;
    }

    /**
     * User token used for the publishing.
     */
    @Input
    @Optional
    public abstract Property<String> getUsername();

    /**
     * Token passphrase used for the publishing.
     */
    @Input
    @Optional
    public abstract Property<String> getPassword();

    private static final int SUCCESS_STATUS_CODE = 201;

    /**
     * Uploads the file via the Central API.
     *
     * @throws IOException        If any errors happen during upload.
     * @throws URISyntaxException If any errors happen when building the URI.
     */
    @TaskAction
    public void sendRequest() throws IOException, URISyntaxException {
        var target = getProject();
        var extension = target.getExtensions()
            .getByType(CentralPortalExtension.class);

        var username = getUsername().getOrNull();
        if (username == null)
            username = extension.getUsername().getOrNull();
        if (username == null)
            username = Objects.toString(
                target.findProperty("centralPortal.username"),
                null
            );
        if (username == null)
            throw new IOException(
                "Missing PublishToCentralPortal's `username`, `centralPortal.username` value"
                + " and `centralPortal.username` property"
            );

        var password = getPassword().getOrNull();
        if (password == null)
            password = extension.getPassword().getOrNull();
        if (password == null)
            password = Objects.toString(
                target.findProperty("centralPortal.password"),
                null
            );
        if (password == null)
            throw new IOException(
                "Missing PublishToCentralPortal's `password`, `centralPortal.password` value"
                + " and `centralPortal.password` property"
            );

        var publishingType = extension.getPublishingType().getOrElse(PublishingType.AUTOMATIC);

        var outputFile = checksumTask.outputFile();
        var nameProvider = extension.getName()
            .orElse(target.getRootProject().getName());
        var bundleName = URLEncoder.encode(
            getProject().getGroup()
            + ":" + nameProvider.get()
            + ":" + getProject().getVersion(),
            UTF_8
        );

        var userPass = username + ":" + password;
        var token = Base64.getEncoder().encodeToString(userPass.getBytes());

        var conn = (HttpURLConnection) // or HttpsURLConnection
            new URI(UPLOAD_ENDPOINT + publishingType + "&name=" + bundleName)
                .toURL()
                .openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "UserToken " + token);
        var boundary = "---------------------------" + Long.toHexString(~currentTimeMillis());
        conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

        try (var out = conn.getOutputStream()) {
            out.write(("--" + boundary).getBytes());
            out.write("\r\nContent-Disposition: form-data; name=\"bundle\"; filename=\"bundle.zip\"".getBytes());
            out.write("\r\nContent-Type: application/octet-stream".getBytes());
            out.write("\r\n\r\n".getBytes());

            try (var inputStream = new FileInputStream(outputFile)) {
                final var buffer = new byte[1024];
                var available = outputFile.length();
                while (available > 0) {
                    var read = inputStream.read(buffer, 0, (int) min(buffer.length, available));
                    out.write(buffer, 0, read);
                    available -= read;
                }
            }

            out.write(("\r\n--" + boundary + "--\r\n").getBytes());
            out.flush();
        }

        var status = conn.getResponseCode();
        switch (status) {
            case SUCCESS_STATUS_CODE -> {
            }
            default -> throw new IOException(
                "Error " + status + ": " + new String(conn.getErrorStream().readAllBytes())
            );
        }
    }
}
